
let token = "819babcd7902454f930c154272296d78";

let urlCompetitions = 'https://api.football-data.org/v2/competitions';
let urlMatches = 'https://api.football-data.org/v2/matches';
let urlTeams = 'https://api.football-data.org/v2/teams';
let urlAreas = 'https://api.football-data.org/v2/areas';
let urlPlayers = 'https://api.football-data.org/v2/players';

/*let competitionsData;// = getSomething(urlCompetitions);
let matchesData;
let teamsData;
let areasData;
let playersData;

let names;*/

/*function getSomething(url) {
    fetch(url, {
        method: "GET",
        headers: {
            "x-auth-token": token
        }
    })
    .then(response => response.json())
    .then(function (data) {
        return data.value;
    })
    return "";
}*/

/*function getCompetitions() {
    fetch(urlCompetitions, {
        method: "GET",
        headers: {
            "x-auth-token": token
        }
    })
    .then(response => response.json())
    .then(function (data) {
        competitions = data;
        let html = "";
        data.competitions.forEach(element => {
            html += "<li>(" + element.id + ")(" + element.area.name + ") " + element.name + "</li>";
            //names.push(element.name);
            console.log(element.name);
        });
        document.getElementById("list").innerHTML = html;
    })
}*/

function getMatches() {
    fetch(urlMatches, {
        method: "GET",
        headers: {
            "x-auth-token": token
        }
    })
    .then(response => response.json())
    .then(function (data) {
        let html = "";
        html += "<table>";
        html += "<tr>"
            + "<th>Wettbewerb</th>"
            + "<th>Heimteam</th>"
            + "<th>Gastteam</th>"
            + "<th>Status</th>"
            + "</tr>";
        data.matches.forEach(element => {
            html += "<tr>" 
                + "<td><data value='" + element.id + "' onclick='getMatch(value)'>" + element.competition.name + "</data></td>"
                + "<td><data value='" + element.id + "' onclick='getMatch(value)'>" + element.homeTeam.name + "</data></td>"
                + "<td><data value='" + element.id + "' onclick='getMatch(value)'>" + element.awayTeam.name + "</data></td>"
                + "<td><data value='" + element.id + "' onclick='getMatch(value)'>" + element.status + "</data></td>"
                + "</tr>";
            console.log("id " + element.id + "  value " + element.competition.name);
        });
        html += "</table>";
        document.getElementById("list").innerHTML = html;
    })
}

function getMatchesLive() {
    fetch(urlMatches + "?status=LIVE", {
        method: "GET",
        headers: {
            "x-auth-token": token
        }
    })
    .then(response => response.json())
    .then(function (data) {
        let html = "";
        html += "<table>";
        html += "<tr>"
            + "<th>Wettbewerb</th>"
            + "<th>Heimteam</th>"
            + "<th>Gastteam</th>"
            + "<th>Akteller Stand</th>"
            + "</tr>";
        data.matches.forEach(element => {
            html += "<tr>" 
                + "<td><data value='" + element.id + "' onclick='getMatch(value)'>" + element.competition.name + "</data></td>"
                + "<td><data value='" + element.id + "' onclick='getMatch(value)'>" + element.homeTeam.name + "</data></td>"
                + "<td><data value='" + element.id + "' onclick='getMatch(value)'>" + element.awayTeam.name + "</data></td>"
                + "<td><data value='" + element.id + "' onclick='getMatch(value)'>" + element.score.fullTime.homeTeam + " vs. " + element.score.fullTime.awayTeam + "</data></td>"
                + "</tr>";
            console.log("id " + element.id + "  value " + element.competition.name);
        });
        html += "</table>";
        document.getElementById("list").innerHTML = html;
    })
}

function getMatch(getid) {
    console.log("returning Match data of " + getid);
    fetch(urlMatches + "/" + getid, {
        method: "GET",
        headers: {
            "x-auth-token": token
        }
    })
    .then(response => response.json())
    .then(function (data) {
        let html = "";
        //competition
        html += "<h1>" + data.match.competition.name;
        //live
        if (data.match.status === "IN_PLAY")
        {
            html += " (live)";
        }
        html += "</h1>";
        //vs
        if (data.match.score.winner === "HOME_TEAM")
        {
            html += "<b>" + data.head2head.homeTeam.name + "</b> vs. " + data.head2head.awayTeam.name;
        }
        else if (data.match.score.winner === "AWAY_TEAM")
        {
            html += data.head2head.homeTeam.name + " vs. <b>" + data.head2head.awayTeam.name + "</b>";
        }
        else
        {
            html += data.head2head.homeTeam.name + " vs. " + data.head2head.awayTeam.name;
        }
        //tore
        if (data.match.status != "SCHEDULED")
        html += "<br>" + data.match.score.fullTime.homeTeam + " vs. " + data.match.score.fullTime.awayTeam;
        console.log(html);
        document.getElementById("list").innerHTML = html;
    })
}

/*
function getTeams() {
    fetch(urlTeams, {
        method: "GET",
        headers: {
            "x-auth-token": token
        }
    })
    .then(response => response.json())
    .then(function (data) {
        let html = "";
        let counter = 0;
        data.teams.forEach(element => {
            html += "<li>(" + element.id + ")(" + element.area.name + ") " + element.name + "</li>";
            console.log("<li>(" + element.id + ")(" + element.area.name + ") " + element.name + "</li>");
            counter++;
        });
        console.log(counter);
        document.getElementById("list").innerHTML = html;
    })
}

function getAreas() {
    fetch(urlAreas, {
        method: "GET",
        headers: {
            "x-auth-token": token
        }
    })
    .then(response => response.json())
    .then(function (data) {
        let html = "";
        let counter = 0;
        data.areas.forEach(element => {
            html += "<li>(" + element.id + ") " + element.name + " in " + element.parentArea + "</li>";
            console.log("<li>(" + element.id + ") " + element.name + " in " + element.parentArea + "</li>");
            counter++;
        });
        console.log(counter);
        document.getElementById("list").innerHTML = html;
    })
}

function suchen() {
    let suchbegriff = document.getElementById("tb-suche").value;
    console.log('Suche: "' + suchbegriff + '"');
    console.log(names);
    html = "";
    names.forEach(element => {
        if (element.includes(suchbegriff))
        {
            html += "saaas";
        }
    }); 
}
*/